# Import Flask components
from flask import session, redirect, url_for, render_template
	
# Import the FlaskView template.
from flask.ext.classy import FlaskView

# Import any other views that this links to.
from .Login import LoginView

class SettingsView(FlaskView):
	def index(self):
		if 'username' in session:
			return render_template('acc.html', session=session)
		else:
			return redirect(url_for("LoginView:index"))
		

