# Import Flask components
from flask import session, redirect, url_for, render_template, flash, request
	
# Import the Database solution
from .ORM.Database.Database import Database	
	
# Import the FlaskView template.
from flask.ext.classy import FlaskView, route

# Import any other views that this links to.
from .Login import LoginView

from .Page import Page

class CarsView(FlaskView):
	page = None
	_db = None

	def before_request(self, name, regno = None):
		self.page = Page()
		
		self.page.name = 'Cars List'
		self.page.styles = ['pages']
		self.page.scripts = ['global']
		self.page.pages['cars']['extend'] = ' class="current"'
		
		self._db = Database()
		
	def after_request(self, name, response, regno = None):
		if self._db is not None:
			self._db.close()
		return response
				
	def index(self):
		if 'name' in session:
			self._db = Database()
			
			if self._db is not None:
				self._db.cursor.execute("SELECT * FROM carsharing.listCars();")
				
				return render_template('cars.html', page=self.page, cars=self._db.cursor)
			else:
				flash('There was a problem processing your query. Please try again some other time.')
		else:
			return redirect(url_for("LoginView:index"))
			
	@route('/<regno>/')
	def get(self, regno):
		if 'name' in session:		
			if self._db is not None:
				#try:
				self._db.cursor.execute("SELECT * FROM carsharing.getCarData(%s);", (regno,))
				cara = self._db.cursor.fetchone()
				if cara:
					self._db.cursor.execute("SELECT * FROM carsharing.getReviewOfCar(%s, %s);", (regno,regno,))
					reviews = self._db.cursor.fetchall()
					
					self._db.cursor.execute("SELECT * FROM carsharing.carTable(CAST(%s AS carsharing.CarRegType));", (regno,))
					times = self._db.cursor.fetchall()
					
					return render_template('carreg.html', page=self.page, car=cara, reviews=reviews, regno=regno, times=times)
				else:
					flash('That car does not seem to exist.')
					return render_template('carreg.html', page=self.page, car=None, reviews=None, times=None) 
				#except:
				#	flash('There was a problem processing your request.');
				#	return render_template('carreg.html', page=self.page, car=None, reviews=None) 
			else:
				flash('There was a problem processing your query. Please try again some other time.')
				return render_template('carreg.html', page=self.page, car=None, reviews=None, times=None)
		else:
			return redirect(url_for("LoginView:index"))
			
	@route('/<regno>/write/')
	def write(self, regno):
		if 'name' in session:		
			return render_template('write.html', page=self.page, regno=regno)
		else:
			return redirect(url_for("LoginView:index"))
			
	def post(self):
		if 'name' in session:		
			if self._db is not None:
				#try:
				self._db.cursor.execute("SELECT * FROM carsharing.writeReview(%s, %s, %s, %s, %s);", (session['memberNo'], session['memberNo'], request.form['regno'], request.form['rating'], request.form['text'],))
				self._db.commit()
				flash('Your review has successfully been publised.')
				#except:
				#	flash('There was a problem submitting your review. Please try again later.')
			else:
				flash('There was a problem processing your query. Please try again some other time.')
		else:
			return redirect(url_for("LoginView:index"))
		
		return redirect(url_for("CarsView:get", regno=request.form['regno']))
					
