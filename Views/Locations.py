# Import Flask components
from flask import session, redirect, url_for, render_template, flash

# Import the Database solution
from .ORM.Database.Database import Database
	
# Import the FlaskView template.
from flask.ext.classy import FlaskView, route

# Import any other views that this links to.
from .Login import LoginView

from .Page import Page

class LocationsView(FlaskView):
	page = None
	_db = None

	def before_request(self, name, locid = None):
		self.page = Page()
		
		self.page.name = 'Pods List'
		self.page.styles = ['pages']
		self.page.scripts = ['global']
		self.page.pages['pods']['extend'] = ' class="current"'
		
		self._db = Database()
		
	def after_request(self, name, response, locid = None):
		if self._db is not None:
			self._db.close()
		return response
				
	def index(self):
		if 'name' in session:
			flash('Invalid page')
			return redirect(url_for("RootView:index"))
		else:
			return redirect(url_for("LoginView:index"))
			
	@route('/<locid>/')
	def get(self, locid):
		if 'name' in session:
			if self._db is not None:
				try:
					self._db.cursor.execute("SELECT * FROM carsharing.getLocData(%s);", (locid,))
					loc = self._db.cursor.fetchone()
				
					self._db.cursor.execute("SELECT * FROM carsharing.getUpperLevelLoc(%s);", (locid,))
					upploc = self._db.cursor.fetchone()
				
					self._db.cursor.execute("SELECT * FROM carsharing.getLowerLevelsLoc(%s);", (locid,))
					lowloc = self._db.cursor.fetchall()
				
					self._db.cursor.execute("SELECT * FROM carsharing.listPodsAtLoc(%s);", (locid,))
					pods = self._db.cursor.fetchall()
				
					return render_template('locations.html', page=self.page, loc=loc, upploc=upploc, sublocs=lowloc, pods=pods)
				except:
					flash('Something has gone wrong. Probably the location you are looking for is incorrect.')
					return render_template('locations.html', page=self.page, loc=None, upploc=None, sublocs=None, pods=None)
			else:
				flash('There was a problem processing your query. Please try again some other time.')
				return render_template('podid.html', page=self.page, pod=None)
		else:
			return redirect(url_for("LoginView:index"))
