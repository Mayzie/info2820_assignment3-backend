# Import Flask components
from flask import session, request, redirect, url_for, render_template, flash

# Import the Database solution
from .ORM.Database.Database import Database

# Import the FlaskView template.
from flask.ext.classy import FlaskView

# Import other stuff
from .Page import Page
import hashlib
import string
import random

class LoginView(FlaskView):
	_db = None
	page = None

	def before_request(self, name):
		self.page = Page()

		self.page.name = 'Log in'
		self.page.styles = ['login']
		self.page.scripts = ['login']

	def before_post(self):
		self._db = Database()
		
	def after_post(self, response):
		self._db.close()
		return response

	def index(self):
		if 'name' in session:
			return redirect(url_for("RootView:index"))
		else:
			return render_template('login.html', page=self.page)

	def post(self):
		if request.method == 'POST':
			self._db.cursor.execute("SELECT * FROM carsharing.getRudimentaryLoginData(%s);", (request.form['username'],))
			result = self._db.cursor.fetchone()
			if result is not None:
				if result[3] == '':	# Check if there is no salt. If there isn't, that means we're using sample data, so we must provide a salt and hash the pw.
					salt = ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(10))	# Generate random salt.
					new_pw = hashlib.sha224((result[2] + salt).encode()).hexdigest()[0:20]	# Hash the password.
					
					self._db.cursor.execute("SELECT * FROM carsharing.updatePassword(%s, CAST(%s AS VARCHAR(20)), CAST(%s AS VARCHAR(10)));", (result[0], new_pw, salt, ))	# Store the new password
					self._db.commit()
					return self.post()	# Try logging in again.
				
				#if request.form['password'] == result[2]:
				if hashlib.sha224((request.form['password'] + result[3]).encode()).hexdigest()[0:20] == result[2]:	# Password accepted?
					session['memberNo'] = result[0]
					session['accountNo'] = result[1]
					session['name'] = ''.join([result[5], ' ', result[7], ',', ' ', result[6]])
					session['username'] = session['name']
					session['homepod'] = result[4]
						
					self._db.cursor.execute("SELECT * FROM carsharing.getHomePod(%s);", str(session['memberNo']))
					result = self._db.cursor.fetchone()
					if result is not None:
						session['home-pod-name'] = result[1]
						session['home-pod-id'] = result[0]
					else:
						session['home-pod-name'] = '<em>None</em>'
						
				else:
					flash('Invalid password')
			else:
				flash('Invalid username')
				
			return redirect(url_for("RootView:index"))


