cfg_username = 'postgres'
cfg_password = ''
cfg_database = 'postgres'
cfg_host = 'localhost'
cfg_port = '5432'

import psycopg2

# Basic Database class. Just makes things a little easier here and there.

class Database:
	_connection = None
	cursor = None

	def __init__(self):
		self._connection = psycopg2.connect(host=cfg_host, port=cfg_port, 
			database=cfg_database, user=cfg_username, password=cfg_password)
		if self._connection is not None:
			self.cursor = self._connection.cursor()
			
	def call(self, proc):
		if self.cursor is not None:
			self.cursor.execute("SELECT * FROM " + proc + ";")

	def commit(self):
		if self._connection is not None:
			self._connection.commit()
			
	def callcommit(self, proc):
		if self.cursor is not None:
			self.call(proc)
			self.commit()
		
	def close(self):
		if self.cursor is not None:
			self.cursor.close()
		if self._connection is not None:
			self._connection.close()
			
	def commitclose(self):
		self.commit()
		self.close()
			
	def __exit__(self, type, value, traceback):
		self.close()
