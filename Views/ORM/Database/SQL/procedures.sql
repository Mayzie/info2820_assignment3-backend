-- View for ease with bookings, member and carname
CREATE OR REPLACE VIEW carsharing.bookingMemberAccount AS (
SELECT B.id as id, B.car as regno, C.name as carName, B.madeby as memberNumber, M.nickname as memberName, A.accountno as accountNo, B.status as Status, B.starttime as starttime, B.endtime as endtime
FROM carsharing.car C JOIN carsharing.booking B ON (C.regno = B.car)
		JOIN carsharing.member M ON (B.madeby = M.memberno) 
		JOIN carsharing.account A ON (M.accountno = A.accountno)
ORDER BY starttime DESC
); 
 
 
-- MAIN PAGE QUERY
CREATE OR REPLACE FUNCTION carsharing.mainPage(argAccountNumber INTEGER) RETURNS TABLE(memberNumber INTEGER, Ok BIGINT, Active BIGINT, Cancelled BIGINT, title VARCHAR(10), givenName VARCHAR(50), familyName VARCHAR(50)) AS $$ 
BEGIN
RETURN QUERY
SELECT B.memberNumber, COUNT(CASE WHEN B.status = 'ok' THEN 1 ELSE NULL END) AS OK, 
		COUNT(CASE WHEN B.status = 'active' THEN 1 ELSE NULL END) AS Active, 
		COUNT(CASE WHEN B.status = 'cancelled' THEN 1 ELSE NULL END) AS Cancelled,
		m.title, m.givenName, m.familyName
		FROM carsharing.bookingMemberAccount B JOIN carsharing.member M ON (B.memberNumber = M.memberno)
		WHERE B.accountNo = argAccountNumber
		GROUP BY B.memberNumber, m.memberNo
		ORDER BY m.title, m.givenName;
END;
$$ 
LANGUAGE plpgsql;
 
 
-- MY BOOKINGS QUERY
CREATE OR REPLACE FUNCTION carsharing.myBookings(argMemberNumber INTEGER) RETURNS TABLE(regno carsharing.CarRegType, carname VARCHAR(40), bookingStartTime TIMESTAMP, bookingEndTime TIMESTAMP) AS $$ 
BEGIN
RETURN QUERY
	SELECT B.regno, B.carname, B.starttime, B.endtime
	FROM carsharing.bookingMemberAccount B
	WHERE B.membernumber = argMemberNumber
	ORDER BY B.starttime DESC;
END;
$$ 
LANGUAGE plpgsql;

-- Retrieves data on the pods
CREATE OR REPLACE FUNCTION carsharing.getPodData(podId INTEGER) RETURNS TABLE(name VARCHAR(80), addr VARCHAR(200), descr TEXT, lat FLOAT, long FLOAT) AS $$
BEGIN
RETURN QUERY
	SELECT p.name, p.addr, p.descr, p.latitude, p.longitude
	FROM carsharing.Pod p
	WHERE p.id = podId
	LIMIT 1;
END;
$$
LANGUAGE plpgsql;

-- List all cars + pods
CREATE OR REPLACE FUNCTION carsharing.listCars() RETURNS TABLE(name VARCHAR(40), regno CarSharing.CarRegType, podId INTEGER, podName VARCHAR(80)) AS $$
BEGIN
RETURN QUERY
	SELECT c.name, c.regno, p.id, p.name
	FROM carsharing.Car c
	INNER JOIN carsharing.Pod p ON p.id = c.parkedAt
	GROUP BY c.regno, p.id;
END;
$$
LANGUAGE plpgsql;

-- Retrieves information about a car.
-- DROP FUNCTION carsharing.getCarData(carsharing.carregtype)
CREATE OR REPLACE FUNCTION getCarData(regono carsharing.CarRegType) RETURNS TABLE (name VARCHAR(40), make VARCHAR(20), model VARCHAR(20), year INTEGER, transmission VARCHAR(6), podName VARCHAR(80), podId INTEGER) AS $$
BEGIN
RETURN QUERY
	SELECT c.name, c.make, c.model, c.year, c.transmission, p.name, p.id
	FROM carsharing.Car c
	INNER JOIN carsharing.Pod p ON p.id = c.parkedAt
	WHERE c.regno = regono
	LIMIT 1;
END;
$$
LANGUAGE plpgsql;

-- MAKE BOOKING STORED PROCEDURE
CREATE OR REPLACE FUNCTION carsharing.makeBooking(memberNum INTEGER, nameOfCar VARCHAR(40), newStart TIMESTAMP, newEnd TIMESTAMP) RETURNS TABLE(bookingNumber INTEGER, memberNumber INTEGER, carName VARCHAR(40), starttime TIMESTAMP, endtime TIMESTAMP) AS $body$
BEGIN
	IF(NOT EXISTS (SELECT 1 
			FROM carsharing.bookingMemberAccount B
			WHERE B.carname = nameOfCar AND (
				(B.starttime BETWEEN newStart AND newEnd)
				OR
				(B.endtime BETWEEN newStart AND newEnd)
			)))
	THEN
		INSERT INTO carsharing.booking(car, madeby, whenbooked, status, starttime, endtime) VALUES (
			(SELECT regno FROM carsharing.car WHERE name=nameOfCar),
			memberNum, 
			CURRENT_TIMESTAMP,
			'active',
			newStart, 
			newEnd
		);
		CREATE OR REPLACE VIEW carsharing.bookingMemberAccount AS (
			SELECT B.id as id, B.car as regno, C.name as carName, B.madeby as memberNumber, M.nickname as memberName, A.accountno as accountNo, B.status as Status, B.starttime as starttime, B.endtime as endtime
			FROM carsharing.car C JOIN carsharing.booking B ON (C.regno = B.car)
					JOIN carsharing.member M ON (B.madeby = M.memberno) 
					JOIN carsharing.account A ON (M.accountno = A.accountno)
			ORDER BY starttime DESC
		);
		RETURN QUERY
		SELECT B.id, B.madeby,  C.name, B.starttime, B.endtime FROM carsharing.booking B JOIN carsharing.car C ON (B.car = C.regno) WHERE B.starttime = newStart AND B.endtime = newEnd AND B.madeby = memberNum;
	ELSE 
		-- there is already a booking within the designated times.
		RETURN QUERY
		SELECT NULL, NULL, NULL, NULL, NULL;
	END IF; 
END;
$body$ LANGUAGE plpgsql;
 
 
-- REVIEWS OF CERTAIN CAR 
-- Get all the reviews on that car
CREATE OR REPLACE FUNCTION carsharing.getReviewOfCar(regono carsharing.CarRegType, regono2 carsharing.CarRegType) RETURNS TABLE(memberGivenName VARCHAR(50), memberFamilyName VARCHAR(50), dateDone DATE, rated carsharing.ratingdomain, description VARCHAR(500)) AS $body$
BEGIN
RETURN QUERY
	SELECT m.givenName, m.familyName, R.whenDone, R.rating, R.description
	FROM carsharing.review R 
	INNER JOIN carsharing.member m ON (m.memberNo = R.memberNo)
	WHERE R.regno = regono;
END;
$body$ LANGUAGE plpgsql;

-- Write a review
CREATE OR REPLACE FUNCTION carsharing.writeReview(argMemberNum INTEGER, argMemberNum2 INTEGER, argCarReg carsharing.CarRegType, userRating carsharing.ratingdomain, userDesc VARCHAR(500)) RETURNS INTEGER AS $body$
BEGIN
	IF(NOT EXISTS(SELECT regno FROM carsharing.car WHERE regno=argCarReg LIMIT 1)) THEN
		RETURN 0;
	ELSE 
		IF EXISTS (SELECT memberNo, regno FROM carsharing.review WHERE memberNo = argMemberNum AND regno = argCarReg LIMIT 1) THEN
			UPDATE carsharing.Review SET rating = userRating, description = userDesc WHERE memberNo = argMemberNum AND regno = argCarReg;
			RETURN 1;
		ELSE
			INSERT INTO carsharing.review VALUES (
				argMemberNum, 
				argCarReg,
				CURRENT_DATE,
				userRating, 
				userDesc
			);
			RETURN 1;
		END IF;
	END IF;
END;
$body$ LANGUAGE plpgsql;

-- Retrieves the next booking
CREATE OR REPLACE FUNCTION carsharing.getNextBooking(argMemberNum INTEGER) RETURNS TABLE(bEndTime TIMESTAMP) AS $$
BEGIN
RETURN QUERY
	SELECT b.endTime
	FROM carsharing.Booking b
	WHERE b.madeBy = argMemberNum
	AND (b.status = 'active')
	AND (b.endTime > now())
	ORDER BY b.endTime;
END;
$$
LANGUAGE plpgsql;

-- Returns little data on the home pod.
CREATE OR REPLACE FUNCTION carsharing.getHomePod(argMemberNum INTEGER) RETURNS TABLE(homePodId INTEGER, homePodName VARCHAR(80)) AS $$
BEGIN
RETURN QUERY
	SELECT p.id, p.name
	FROM carsharing.Pod p
	INNER JOIN carsharing.Member m ON m.homePod = p.id
	WHERE m.memberNo = argMemberNum
	LIMIT 1;
END;
$$
LANGUAGE plpgsql;

-- Retrieves data to log users in and apply to the session
CREATE OR REPLACE FUNCTION carsharing.getRudimentaryLoginData(nick VARCHAR(10)) 
	RETURNS TABLE(memberNo INTEGER, accountNo INTEGER, passwd VARCHAR(20), pw_salt VARCHAR(10), homePod INTEGER, title VARCHAR(10), givenName VARCHAR(50), familyName VARCHAR(50))
AS $$
BEGIN
RETURN QUERY
	SELECT m.memberNo, m.accountNo, m.passwd, m.pw_salt, m.homePod, m.title, m.givenName, m.familyName
	FROM carsharing.Member m
	WHERE nickname = nick
	LIMIT 1;
END;
$$
LANGUAGE plpgsql;

-- Replaces the plaintext password with a salt.
CREATE OR REPLACE FUNCTION carsharing.updatePassword(argMemberNo INTEGER, new_passwd VARCHAR(20), new_salt VARCHAR(10)) RETURNS INTEGER AS $$
BEGIN
	UPDATE carsharing.Member SET passwd = new_passwd, pw_salt = new_salt WHERE memberNo = argMemberNo;
	RETURN 0;
END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION carsharing.makeRating(argMemberNum INTEGER, argReviewedMember INTEGER, carRegNo CHAR(6), argUsefulnes SMALLINT) RETURNS INTEGER AS $body$
BEGIN
	IF EXISTS(SELECT * FROM carsharing.review R WHERE R.memberno = argReviewedMember AND R.regno=carRegNo) THEN
		INSERT INTO carsharing.rating VALUES (
			argMemberNum, 
			reviewedMember, 
			carRegNo, 
			argUsefulness,
			CURRENT_DATE
		);
		RETURN 1;
	ELSE 
		RETURN 0;
	END IF;
END;
$body$
LANGUAGE plpgsql;


---


-- View for ease with bookings, member and carname
CREATE OR REPLACE VIEW carsharing.bookingMemberAccount AS (
SELECT B.id as id, B.car as regno, C.name as carName, B.madeby as memberNumber, M.nickname as memberName, A.accountno as accountNo, B.status as Status, B.starttime as starttime, B.endtime as endtime
FROM carsharing.car C JOIN carsharing.booking B ON (C.regno = B.car)
		JOIN carsharing.member M ON (B.madeby = M.memberno) 
		JOIN carsharing.account A ON (M.accountno = A.accountno)
ORDER BY starttime DESC
); 
 
 
-- MAIN PAGE QUERY
CREATE OR REPLACE FUNCTION carsharing.mainPage(argAccountNumber INTEGER) RETURNS TABLE(memberNumber INTEGER, Ok BIGINT, Active BIGINT, Cancelled BIGINT, title VARCHAR(10), givenName VARCHAR(50), familyName VARCHAR(50)) AS $$ 
BEGIN
RETURN QUERY
SELECT B.memberNumber, COUNT(CASE WHEN B.status = 'ok' THEN 1 ELSE NULL END) AS OK, 
		COUNT(CASE WHEN B.status = 'active' THEN 1 ELSE NULL END) AS Active, 
		COUNT(CASE WHEN B.status = 'cancelled' THEN 1 ELSE NULL END) AS Cancelled,
		m.title, m.givenName, m.familyName
		FROM carsharing.bookingMemberAccount B JOIN carsharing.member M ON (B.memberNumber = M.memberno)
		WHERE B.accountNo = argAccountNumber
		GROUP BY B.memberNumber, m.memberNo
		ORDER BY m.title, m.givenName;
END;
$$ 
LANGUAGE plpgsql;
 
 
-- MY BOOKINGS QUERY
CREATE OR REPLACE FUNCTION carsharing.myBookings(argMemberNumber INTEGER) RETURNS TABLE(regno carsharing.CarRegType, carname VARCHAR(40), bookingStartTime TIMESTAMP, bookingEndTime TIMESTAMP) AS $$ 
BEGIN
RETURN QUERY
	SELECT B.regno, B.carname, B.starttime, B.endtime
	FROM carsharing.bookingMemberAccount B
	WHERE B.membernumber = argMemberNumber
	ORDER BY B.starttime DESC;
END;
$$ 
LANGUAGE plpgsql;

-- Retrieves data on the pods
CREATE OR REPLACE FUNCTION carsharing.getPodData(podId INTEGER) RETURNS TABLE(name VARCHAR(80), addr VARCHAR(200), descr TEXT, lat FLOAT, long FLOAT) AS $$
BEGIN
RETURN QUERY
	SELECT p.name, p.addr, p.descr, p.latitude, p.longitude
	FROM carsharing.Pod p
	WHERE p.id = podId
	LIMIT 1;
END;
$$
LANGUAGE plpgsql;

-- List all cars + pods
CREATE OR REPLACE FUNCTION carsharing.listCars() RETURNS TABLE(name VARCHAR(40), regno CarSharing.CarRegType, podId INTEGER, podName VARCHAR(80)) AS $$
BEGIN
RETURN QUERY
	SELECT c.name, c.regno, p.id, p.name
	FROM carsharing.Car c
	INNER JOIN carsharing.Pod p ON p.id = c.parkedAt
	GROUP BY c.regno, p.id;
END;
$$
LANGUAGE plpgsql;

-- Retrieves information about a car.
-- DROP FUNCTION carsharing.getCarData(carsharing.carregtype)
CREATE OR REPLACE FUNCTION getCarData(regono carsharing.CarRegType) RETURNS TABLE (name VARCHAR(40), make VARCHAR(20), model VARCHAR(20), year INTEGER, transmission VARCHAR(6), podName VARCHAR(80), podId INTEGER) AS $$
BEGIN
RETURN QUERY
	SELECT c.name, c.make, c.model, c.year, c.transmission, p.name, p.id
	FROM carsharing.Car c
	INNER JOIN carsharing.Pod p ON p.id = c.parkedAt
	WHERE c.regno = regono
	LIMIT 1;
END;
$$
LANGUAGE plpgsql;

-- MAKE BOOKING STORED PROCEDURE
CREATE OR REPLACE FUNCTION carsharing.makeBooking(memberNum INTEGER, nameOfCar VARCHAR(40), newStart TIMESTAMP, newEnd TIMESTAMP) RETURNS TABLE(bookingNumber INTEGER, memberNumber INTEGER, carName VARCHAR(40), starttime TIMESTAMP, endtime TIMESTAMP) AS $body$
BEGIN
	IF(NOT EXISTS (SELECT 1 
			FROM carsharing.bookingMemberAccount B
			WHERE B.carname = nameOfCar AND (
				(B.starttime BETWEEN newStart AND newEnd)
				OR
				(B.endtime BETWEEN newStart AND newEnd)
			)))
	THEN
		INSERT INTO carsharing.booking(car, madeby, whenbooked, status, starttime, endtime) VALUES (
			(SELECT regno FROM carsharing.car WHERE name=nameOfCar),
			memberNum, 
			CURRENT_TIMESTAMP,
			'active',
			newStart, 
			newEnd
		);
		CREATE OR REPLACE VIEW carsharing.bookingMemberAccount AS (
			SELECT B.id as id, B.car as regno, C.name as carName, B.madeby as memberNumber, M.nickname as memberName, A.accountno as accountNo, B.status as Status, B.starttime as starttime, B.endtime as endtime
			FROM carsharing.car C JOIN carsharing.booking B ON (C.regno = B.car)
					JOIN carsharing.member M ON (B.madeby = M.memberno) 
					JOIN carsharing.account A ON (M.accountno = A.accountno)
			ORDER BY starttime DESC
		);
		RETURN QUERY
		SELECT B.id, B.madeby,  C.name, B.starttime, B.endtime FROM carsharing.booking B JOIN carsharing.car C ON (B.car = C.regno) WHERE B.starttime = newStart AND B.endtime = newEnd AND B.madeby = memberNum;
	ELSE 
		-- there is already a booking within the designated times.
		RETURN QUERY
		SELECT NULL, NULL, NULL, NULL, NULL;
	END IF; 
END;
$body$ LANGUAGE plpgsql;
 
 
-- REVIEWS OF CERTAIN CAR 
-- Get all the reviews on that car
DROP FUNCTION carsharing.getreviewofcar(carsharing.carregtype,carsharing.carregtype);
CREATE OR REPLACE FUNCTION carsharing.getReviewOfCar(regono carsharing.CarRegType, regono2 carsharing.CarRegType) RETURNS TABLE(memberGivenName VARCHAR(50), memberFamilyName VARCHAR(50), dateDone DATE, rated carsharing.ratingdomain, description VARCHAR(500)) AS $body$
BEGIN
RETURN QUERY
	SELECT m.givenName, m.familyName, R.whenDone, R.rating, R.description
	FROM carsharing.review R 
	INNER JOIN carsharing.member m ON (m.memberNo = R.memberNo)
	WHERE R.regno = regono;
END;
$body$ LANGUAGE plpgsql;

-- Write a review
CREATE OR REPLACE FUNCTION carsharing.writeReview(argMemberNum INTEGER, argMemberNum2 INTEGER, argCarReg carsharing.CarRegType, userRating carsharing.ratingdomain, userDesc VARCHAR(500)) RETURNS INTEGER AS $body$
BEGIN
	IF(NOT EXISTS(SELECT regno FROM carsharing.car WHERE regno=argCarReg LIMIT 1)) THEN
		RETURN 0;
	ELSE 
		IF EXISTS (SELECT memberNo, regno FROM carsharing.review WHERE memberNo = argMemberNum AND regno = argCarReg LIMIT 1) THEN
			UPDATE carsharing.Review SET rating = userRating, description = userDesc WHERE memberNo = argMemberNum AND regno = argCarReg;
			RETURN 1;
		ELSE
			INSERT INTO carsharing.review VALUES (
				argMemberNum, 
				argCarReg,
				CURRENT_DATE,
				userRating, 
				userDesc
			);
			RETURN 1;
		END IF;
	END IF;
END;
$body$ LANGUAGE plpgsql;

-- Retrieves the next booking
CREATE OR REPLACE FUNCTION carsharing.getNextBooking(argMemberNum INTEGER) RETURNS TABLE(bEndTime TIMESTAMP) AS $$
BEGIN
RETURN QUERY
	SELECT b.endTime
	FROM carsharing.Booking b
	WHERE b.madeBy = argMemberNum
	AND (b.status = 'active')
	AND (b.endTime > now())
	ORDER BY b.endTime;
END;
$$
LANGUAGE plpgsql;

-- Returns little data on the home pod.
CREATE OR REPLACE FUNCTION carsharing.getHomePod(argMemberNum INTEGER) RETURNS TABLE(homePodId INTEGER, homePodName VARCHAR(80)) AS $$
BEGIN
RETURN QUERY
	SELECT p.id, p.name
	FROM carsharing.Pod p
	INNER JOIN carsharing.Member m ON m.homePod = p.id
	WHERE m.memberNo = argMemberNum
	LIMIT 1;
END;
$$
LANGUAGE plpgsql;

-- Retrieves data to log users in and apply to the session
CREATE OR REPLACE FUNCTION carsharing.getRudimentaryLoginData(nick VARCHAR(10)) 
	RETURNS TABLE(memberNo INTEGER, accountNo INTEGER, passwd VARCHAR(20), pw_salt VARCHAR(10), homePod INTEGER, title VARCHAR(10), givenName VARCHAR(50), familyName VARCHAR(50))
AS $$
BEGIN
RETURN QUERY
	SELECT m.memberNo, m.accountNo, m.passwd, m.pw_salt, m.homePod, m.title, m.givenName, m.familyName
	FROM carsharing.Member m
	WHERE nickname = nick
	LIMIT 1;
END;
$$
LANGUAGE plpgsql;

-- Replaces the plaintext password with a salt.
CREATE OR REPLACE FUNCTION carsharing.updatePassword(argMemberNo INTEGER, new_passwd VARCHAR(20), new_salt VARCHAR(10)) RETURNS INTEGER AS $$
BEGIN
	UPDATE carsharing.Member SET passwd = new_passwd, pw_salt = new_salt WHERE memberNo = argMemberNo;
	RETURN 0;
END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION carsharing.makeRating(argMemberNum INTEGER, argReviewedMember INTEGER, carRegNo CHAR(6), argUsefulnes SMALLINT) RETURNS INTEGER AS $body$
BEGIN
	IF EXISTS(SELECT * FROM carsharing.review R WHERE R.memberno = argReviewedMember AND R.regno=carRegNo) THEN
		INSERT INTO carsharing.rating VALUES (
			argMemberNum, 
			reviewedMember, 
			carRegNo, 
			argUsefulness,
			CURRENT_DATE
		);
		RETURN 1;
	ELSE 
		RETURN 0;
	END IF;
END;
$body$
LANGUAGE plpgsql;

-- Returns the hourly and daily rate for a member.
CREATE OR REPLACE FUNCTION carsharing.getHourlyDailyRate(argMemberNum INTEGER) RETURNS TABLE (h_rate carsharing.AmountInCents, d_rate carsharing.AmountInCents) AS $$
BEGIN
RETURN QUERY
	SELECT hourly_rate, daily_rate
	FROM carsharing.MembershipPlan p
	INNER JOIN carsharing.Account a ON a.plan = p.title
	INNER JOIN carsharing.Member m ON m.accountNo = a.accountNo
	WHERE m.memberNo = 4
	LIMIT 1;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION carsharing.getUpperLevelLoc(argCurrentLocId INTEGER) RETURNS TABLE (id INTEGER, name VARCHAR(80), atype VARCHAR(10)) AS $$
BEGIN
RETURN QUERY
	SELECT l2.id, l2.name, l2.type
	FROM carsharing.location l1
	INNER JOIN carsharing.location l2 ON l2.id = l1.partOf
	WHERE l1.id = argCurrentLocId
	LIMIT 1;
END;
$$ LANGUAGE  plpgsql;

CREATE OR REPLACE FUNCTION carsharing.getLowerLevelsLoc(argCurrentLocId INTEGER) RETURNS TABLE (id INTEGER, name VARCHAR(80), atype VARCHAR(10)) AS $$
BEGIN
RETURN QUERY
	SELECT l.id, l.name, l.type
	FROM carsharing.location l
	WHERE l.partOf = argCurrentLocId;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION carsharing.listPods() RETURNS TABLE (podId INTEGER, podName VARCHAR(80), locId INTEGER, locName VARCHAR(80)) AS $$
BEGIN
RETURN QUERY
	SELECT p.id, p.name, l.id, l.name
	FROM carsharing.pod p
	INNER JOIN carsharing.location l ON l.id = p.isAt;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION carsharing.getLocData(locId INTEGER) RETURNS TABLE (locName VARCHAR(80), locType VARCHAR(10)) AS $$
BEGIN
RETURN QUERY
	SELECT l.name, l.type
	FROM carsharing.Location l
	WHERE l.id = locId
	LIMIT 1;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION carsharing.listPodsAtLoc(locId INTEGER) RETURNS TABLE (podId INTEGER, podName VARCHAR(80)) AS $$
BEGIN
RETURN QUERY
	SELECT p.id, p.name
	FROM carsharing.Pod p
	INNER JOIN carsharing.Location l ON l.id = p.isAt
	WHERE p.isAt = locId;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION carsharing.getLocAtPod(podId INTEGER) RETURNS TABLE (locId INTEGER, locName VARCHAR(80)) AS $$
BEGIN
RETURN QUERY
	SELECT l.id, l.name
	FROM carsharing.Location l
	INNER JOIN carsharing.Pod p ON p.isAt = l.id
	WHERE p.id = podId;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION carsharing.getCarsAtPod(podId INTEGER) RETURNS TABLE (carRegNo carsharing.CarRegType, carName VARCHAR(40), carTransmission VARCHAR(6)) AS $$
BEGIN
RETURN QUERY
	SELECT c.regno, c.name, c.transmission
	FROM carsharing.Car c
	WHERE c.parkedAt = podId;
END;
$$ LANGUAGE plpgsql;

--Check if the current car is available within the day (0:00 to 23:00)
CREATE OR REPLACE FUNCTION carsharing.carTable(argCarReg carsharing.CarRegType) RETURNS TABLE (time_hour TIMESTAMP, avail INTEGER) AS $$
BEGIN
	FOR i IN 0..23 LOOP
		RETURN QUERY 
			SELECT DISTINCT (CURRENT_DATE + (i || 'hour')::interval) AS time_hour, (SELECT (CASE WHEN ((CURRENT_DATE + (i || 'hour')::interval) NOT BETWEEN B.starttime AND B.endtime) THEN 1 ELSE 0 END))
			FROM carsharing.bookingMemberAccount B
			WHERE B.regno = argCarReg;
	END LOOP;
END;
$$ LANGUAGE plpgsql;
