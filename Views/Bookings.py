# Import Flask components
from flask import session, redirect, url_for, render_template

# Import the Database solution
from .ORM.Database.Database import Database
	
# Import the FlaskView template.
from flask.ext.classy import FlaskView

# Import any other views that this links to.
from .Login import LoginView

from .Page import Page

class BookingsView(FlaskView):
	page = None
	_db = None

	def before_request(self, name):
		self.page = Page()
		
		self.page.name = 'Current Bookings'
		self.page.styles = ['pages', 'my-booking']
		self.page.scripts = ['global']
		self.page.pages['bookings']['extend'] = ' class="current"'
		
		self._db = Database()
		
	def after_request(self, name, response):
		if self._db is not None:
			self._db.close()
		return response
				
	def index(self):
		if 'username' in session:
			if self._db is not None:
				self._db.cursor.execute("SELECT * FROM carsharing.myBookings(%s);", (session['memberNo'],))
			return render_template('mybook.html', page=self.page, bookings=self._db.cursor)
		else:
			return redirect(url_for("LoginView:index"))
	
