# Import Flask components
from flask import session, redirect, url_for, render_template, flash

# Import the Database solution
from .ORM.Database.Database import Database
	
# Import the FlaskView template.
from flask.ext.classy import FlaskView, route

# Import any other views that this links to.
from .Login import LoginView

from .Page import Page

class PodsView(FlaskView):
	page = None
	_db = None

	def before_request(self, name, podid = None):
		self.page = Page()
		
		self.page.name = 'Pods List'
		self.page.styles = ['pages']
		self.page.scripts = ['global']
		self.page.pages['pods']['extend'] = ' class="current"'
		
		self._db = Database()
		
	def after_request(self, name, response, podid = None):
		if self._db is not None:
			self._db.close()
		return response
				
	def index(self):
		if 'name' in session:
			if self._db is not None:
				self._db.cursor.execute("SELECT * FROM carsharing.listPods();")
				return render_template('pods.html', page=self.page, pods=self._db.cursor.fetchall())
		else:
			return redirect(url_for("LoginView:index"))
			
	@route('/<podid>/')
	def get(self, podid):
		if 'name' in session:
			if self._db is not None:
				#try:
				self._db.cursor.execute("SELECT * FROM carsharing.getPodData(%s);", (podid,))
				pods = self._db.cursor.fetchone()
				self._db.cursor.execute("SELECT * FROM carsharing.getLocAtPod(%s);", (podid,))
				loc = self._db.cursor.fetchone()
				self._db.cursor.execute("SELECT * FROM carsharing.getCarsAtPod(CAST(%s AS INTEGER));", (podid,))
				cars = self._db.cursor.fetchall()
				return render_template('podid.html', page=self.page, pod=pods, loc=loc, cars=cars)
				#except:
					#flash('That pod does not exist.')
					#return render_template('podid.html', page=self.page, pod=None, loc=None, cars=None)
			else:
				flash('There was a problem processing your query. Please try again some other time.')
				return render_template('podid.html', page=self.page, pod=None, loc=None, cars=None)
		else:
			return redirect(url_for("LoginView:index"))
