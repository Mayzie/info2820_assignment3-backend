# Import Flask components
from flask import session, redirect, url_for, render_template
	
# Import the FlaskView template.
from flask.ext.classy import FlaskView

# Import any other views that this links to.
from .Login import LoginView

class LogoutView(FlaskView):
	def index(self):
		session.clear()	# Clears the session.
		return redirect(url_for("LoginView:index"))
		

