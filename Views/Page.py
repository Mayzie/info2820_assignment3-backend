from collections import OrderedDict

# Reduce redundancy by keeping commonalities between pages in here (e.g. the menu bar)

class Page:
	name = None
	styles = None
	scripts = None
	pages = None
	
	def __init__(self):
		self.styles = []
		self.scripts = []
		
		self.pages = OrderedDict([
			('index', {'url' : '/', 'extend' : '', 'name' : 'Main Page'}),
			('makebooking',  {'url' : '/makebooking/', 'extend' : '', 'name' : 'Make Booking'}),
			('bookings', {'url' : '/bookings/', 'extend' : '', 'name' : 'My Bookings'}),
			('cars', {'url' : '/cars/', 'extend' : '', 'name' : 'Cars'}),				
			('pods', {'url' : '/pods/', 'extend' : '', 'name' : 'Pods'}),])
