# Import Flask components
from flask import session, redirect, url_for, render_template, request, flash
	
# Import the FlaskView template.
from flask.ext.classy import FlaskView, route

# Import the Database solution
from .ORM.Database.Database import Database

# Import any other views that this links to.
from .Login import LoginView
from .Bookings import BookingsView

from .Page import Page

class MakeBookingView(FlaskView):
	page = None
	_db = None

	def before_request(self, name):
		self.page = Page()
		
		self.page.name = 'Vehicle Sharing'
		self.page.styles = ['pages', 'booking', 'jquery.datetimepicker']
		self.page.scripts = ['global', 'jquery.datetimepicker', 'datetimesettings']
		self.page.pages['makebooking']['extend'] = ' class="current"'
		
	def before_post(self):
		self._db = Database()
		
	def after_post(self, response):
		self._db.close()
		return response
				
	def index(self):
		if 'name' in session:
			return render_template('book.html', page=self.page)
		else:
			return redirect(url_for("LoginView:index"))
		
	def post(self):
		if request.method == 'POST':
			if 'car' in request.form:
				if 'stime' in request.form:
					if 'etime' in request.form:
						try:
							self._db.cursor.execute("SELECT * FROM carsharing.makeBooking(%s, CAST(%s AS VARCHAR(40)), %s::TIMESTAMP, %s::TIMESTAMP);", (session['memberNo'], request.form['car'].strip(), request.form['stime'].strip(), request.form['etime'].strip(),))
						except:
							flash('You have entered invalid data')
							return render_template('book.html', page=self.page)
			
						#return "Rawr!"
						if self._db.cursor.fetchone():
							self._db.cursor.execute("SELECT * FROM carsharing.car c WHERE c.name = %s LIMIT 1;", (request.form['car'],))
							if self._db.cursor.fetchone():
								#self._db.cursor.execute("
								flash('Your booking has been successful')
								self._db.commit()	# Permanently save the changes to the database
								return redirect(url_for("MakeBookingView:confirm"))
							else:
								flash('The car you entered does not exist')
						else:
							flash('Sorry, the car you entered is not available at this date/time')
					else:
						flash('You have left out the end date')
				else:
					flash('You have left out the start time')
			else:
				flash('You forgot to fill out the car field.')
				
		return render_template('book.html', page=self.page)
		
	@route('/receipt/')
	def confirm(self):
		if 'name' in session:
			try:
				self._db = Database()
				self._db.cursor.execute("SELECT * FROM carsharing.confirmationPage(%s);", (session['memberNo'],))
			
				result = self._db.cursor.fetchone()
				return render_template('confirmation.html', page=self.page, result=result)
			except:
				flash('Something went wrong. Your booking might not have been saved. Check your My Bookings page to make sure.')
				return render_template('confirmation.html', page=self.page, result=None)
		else:
			return redirect(url_for("LoginView:index"))
