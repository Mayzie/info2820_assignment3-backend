# Import the Flask 
from flask import Flask

# Import the views we want to use
from Views.Root import RootView		# URL: /
from Views.Login import LoginView	# URL: /login/
from Views.Bookings import BookingsView	# URL: /bookings/
from Views.MakeBooking import MakeBookingView	# URL: /makebooking/
from Views.Cars import CarsView		# URL: /cars/
from Views.Pods import PodsView		# URL: /cars/
from Views.Logout import LogoutView	# URL: /logout/
from Views.Locations import LocationsView # URL: /locations/

# Initialsie the Flask application
app = Flask(__name__)
app.config['PROPAGATE_EXCEPTIONS'] = True
app.secret_key = 'RaNdOm-EnCrYpTiOn-KeY'

# Register views/URLs
RootView.register(app)
LoginView.register(app)
BookingsView.register(app)
MakeBookingView.register(app)
CarsView.register(app)
PodsView.register(app)
LogoutView.register(app)
LocationsView.register(app)

if __name__ == '__main__':
	app.run(debug=True)
